import feedparser
import re

import colorama
from colorama import Fore
colorama.init()

url = ["http://showrss.info/show/", ".rss"]

def getEpisode(showId, season=None, episode=None):

    d = feedparser.parse(url[0] + str(showId) + url[1])

    if season == None or episode == None:
        title = d["entries"][0]["tv_raw_title"]
        magnet = d["entries"][0]["links"][1]["href"]
        return {"title": title, "magnet": magnet, "response": "Getting latest episode.", "status": 0}
    else:
        secode = "S" + str(season).zfill(2) + "E" + str(episode).zfill(2)
        for ep in d["entries"]:
            if secode in ep["tv_raw_title"]:
                title = ep["tv_raw_title"]
                magnet = ep["links"][1]["href"]
                return {"title": title, "magnet": magnet, "response": "Getting episode "+secode, "status": 0}
        return {"message": "Not found.", "status": 1}

def parseSeCode(secode):
    secodeRegex = re.compile('s(\d{0,3})e(\d{0,3})', re.I)
    print("[module] [showrss] [parseSeCode]", secodeRegex.match(secode).groups())
    return secodeRegex.match(secode).groups()

if __name__ == '__main__':
    while True:
        parseSeCode(input("> "))
        print()
else:
    print(Fore.BLUE, "[module]",__name__, "module loaded", Fore.RESET)
