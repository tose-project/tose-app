import os, subprocess
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/api/<serie>')
def api_serie(serie):
    return "Serie: " + serie

@app.route('/api/<serie>/<season>/')
def api_serie_season(serie, season):
    return "Serie: " + serie + "<br />Season: " + season

@app.route('/api/<serie>/<season>/<episode>')
def api_serie_season_episode(serie, season, episode):
    returnVar = ""
    # returnVar += "Serie: " + serie + "<br />Season: " + season + "<br />Episode: " + episode + "<br />"
    returnVar += subprocess.check_output(["python", "scriptapi.py", serie, "-s", season, "-e", episode], universal_newlines=True)
    return returnVar


if __name__ == '__main__':
    app.debug = True
    app.run(host=os.environ['OPENSHIFT_PYTHON_IP'], port=int(os.environ['OPENSHIFT_PYTHON_PORT']))