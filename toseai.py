#!python3
import ai_hangouts as hangouts
import showrss
import time, json
import asyncio
import hangups

import configparser
config = configparser.ConfigParser()
config.read('tose.config')
if "apiai" not in config or not config["apiai"]["CLIENT_ACCESS_TOKEN"]:
    print("Please store your Api.ai client token in 'tose.config'. See 'tose.config.example' for structure")
    exit()

import apiai
apiAi = apiai.ApiAI(config["apiai"]["CLIENT_ACCESS_TOKEN"])
reqIncomplete = False

import colorama
from colorama import Fore
colorama.init()

if __name__ == '__main__':
    hangouts.init()

helptext = ["I'm ToSeBot. Talk to me like I'm human and you will be rewarded with magnet (links).",
            "To start interacting with me, start a message with 'tose'. If I ask you something, just reply." ]


def onNewMessage(msg, cid):
    global reqIncomplete
    print(Fore.GREEN, "[ info ]", "Message captured:", msg, "reqIncomplete: ", reqIncomplete, Fore.RESET)

    if "status" in msg.lower():
        hangouts.sendHangoutsMessage("Running smooth.", cid)

    if "cid" in msg.lower():
        print(Fore.GREEN, "[ info ]", "Sending message: ", cid, Fore.RESET)
        hangouts.sendHangoutsMessage(cid,cid)

    if reqIncomplete == False:
        if ( msg.lower().startswith("tose") ):
            asyncio.async(hangouts.set_typing(cid))
            processMsg(msg[5:], cid)
    else:
        asyncio.async(hangouts.set_typing(cid))
        processMsg(msg, cid)


def aiRequest(msg, cid):
    request = apiAi.text_request()
    request.session_id = cid
    request.query = msg
    return json.loads(request.getresponse().read())


def parseAiResponse(response, cid):
    global reqIncomplete

    # Not a specific tose thing
    if ("result" not in response) or ("actionIncomplete" not in response["result"]) or ("score" in response):
        return ""
    if (response["result"]["actionIncomplete"]):
        reqIncomplete = True
        return None
    else:
        reqIncomplete = False

    if response["result"]["action"] == "help":
        return helptext
    elif response["result"]["action"] == "latest_info":
        serie = response["result"]["parameters"]["series"]
        showrssData = showrss.getEpisode(serie)
        if showrssData["status"] == 0:
            return [showrssData["title"], showrssData["magnet"]]
        else:
            return showrssData["message"]

    elif response["result"]["action"] == "shorthand_query":
        serie = response["result"]["parameters"]["series"]
        secode = response["result"]["parameters"]["secode"]
        season, episode = showrss.parseSeCode(secode)
        showrssData = showrss.getEpisode(serie, season=season, episode=episode)
        if showrssData["status"] == 0:
            return [showrssData["title"], showrssData["magnet"]]
        else:
            return showrssData["message"]

    elif response["result"]["action"] == "specific_episode":
        serie = response["result"]["parameters"]["series"]
        season = response["result"]["parameters"]["season"]
        episode = response["result"]["parameters"]["episode"]
        showrssData = showrss.getEpisode(serie, season=season, episode=episode)
        if showrssData["status"] == 0:
            return [showrssData["title"], showrssData["magnet"]]
        else:
            return showrssData["message"]


def processMsg(msg, cid):
    sendResponse = aiRequest(msg, cid)
    print(Fore.YELLOW, "[apiai]", "Response:", str(sendResponse), Fore.RESET)
    sendText = parseAiResponse(sendResponse, cid)
    if sendText:
        print(Fore.YELLOW, "[apiai]", "Parsed Response:", str(sendText), Fore.RESET)
    hangouts.sendHangoutsMessage(str(sendResponse["result"]["fulfillment"]["speech"]), cid)
    if type(sendText) is list:
        for message in sendText:
            hangouts.sendHangoutsMessage(str(message), cid)
    else:
        hangouts.sendHangoutsMessage(str(sendText), cid)
