#!python3
import asyncio
import hangups
from sys import argv
import time

# import logging
# logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

import colorama
from colorama import Fore
colorama.init()

from google.protobuf import descriptor_pb2
from hangups import hangouts_pb2
import toseai as toseai
from hangups.hangouts_pb2 import (
    TYPING_TYPE_STARTED, TYPING_TYPE_PAUSED, TYPING_TYPE_STOPPED,
    CLIENT_PRESENCE_STATE_DESKTOP_IDLE, CLIENT_PRESENCE_STATE_DESKTOP_ACTIVE, FOCUS_TYPE_FOCUSED, FOCUS_DEVICE_DESKTOP
)

## Some conversation IDs for testing.
# Rithvik Vibhu: UgzTQ7JmCpWG_Peinjx4AaABAagB_IuSBQ
# ToSE group:  UgyT6DYhh50bUOijMVh4AaABAQ
# ToSE DevTest group: Ugx56o6iNATAA80XrKp4AaABAQ
# Test group: Ugw-YMKhMfDDCpS7KiV4AaABAQ

REFRESH_TOKEN_PATH = 'refresh_token.txt'


@asyncio.coroutine
def on_state_update(state_update):
    global client
    global reqIncomplete
    # print(state_update)
    if (state_update.conversation.conversation_id.id or state_update.focus_notification.conversation_id.id or state_update.typing_notification.conversation_id.id or state_update.watermark_notification.conversation_id.id):
        asyncio.async(set_focus(state_update.conversation.conversation_id.id or state_update.focus_notification.conversation_id.id or state_update.typing_notification.conversation_id.id or state_update.watermark_notification.conversation_id.id))
    if state_update.HasField('conversation'):
        CONVERSATION_ID = state_update.conversation.conversation_id.id
        msg = state_update.event_notification.event.chat_message.message_content.segment[0].text
        if(state_update.event_notification.event.sender_id.chat_id not in [state_update.event_notification.event.self_event_state.user_id.chat_id, "111392744372361608438"]):
            toseai.onNewMessage(msg, CONVERSATION_ID)


def sendHangoutsMessage(msg, cid=None):
    if cid == None:
        cid = 'Ugx56o6iNATAA80XrKp4AaABAQ'
    cid = cid.replace("Dev","Ugx56o6iNATAA80XrKp4AaABAQ")
    cid = cid.replace("ToSE","UgyT6DYhh50bUOijMVh4AaABAQ")
    cid = cid.replace("Rithvik","UgzTQ7JmCpWG_Peinjx4AaABAagB_IuSBQ")

    print(Fore.GREEN, "[module] [hangouts]", "Sending hangouts message: ", msg, "\nTo: ",cid, Fore.RESET)
    asyncio.async(_send_message(msg, cid))


def _send_message(msg, cid=None):
    global client
    request = hangups.hangouts_pb2.SendChatMessageRequest(
        request_header=client.get_request_header(),
        event_request_header=hangups.hangouts_pb2.EventRequestHeader(
            conversation_id=hangups.hangouts_pb2.ConversationId(
                id=cid
            ),
            client_generated_id=client.get_client_generated_id(),
        ),
        message_content=hangups.hangouts_pb2.MessageContent(
            segment=[hangups.ChatMessageSegment(msg).serialize()],
        ),
    )
    try:
        # Make the request to the Hangouts API.
        yield from client.send_chat_message(request)
    except Exception as e:
        print(Fore.RED, "[ERROR] [module] [hangouts]", "Error while sending: ", e, Fore.RESET)
    finally:
        print(Fore.GREEN, "[module] [hangouts]", "Message sent", Fore.RESET)


@asyncio.coroutine
def set_typing(cid, typing=TYPING_TYPE_STARTED):
    global client
    try:
        yield from client.set_typing(
            hangouts_pb2.SetTypingRequest(
                request_header=client.get_request_header(),
                conversation_id=hangouts_pb2.ConversationId(id=cid),
                type=typing,
            )
        )
    except Exception as e:
        print(Fore.RED, "[ERROR] [module] [hangouts]", "Error in typing: ", e, Fore.RESET)


@asyncio.coroutine
def set_presence_old(cid, set_presence_request=CLIENT_PRESENCE_STATE_DESKTOP_IDLE):
    global client
    response = hangouts_pb2.SetPresenceResponse()
    try:
        yield from client.set_presence(
            hangouts_pb2.SetPresenceRequest(
                request_header=client.get_request_header(),
                presence_state_setting=hangouts_pb2.PresenceStateSetting(type=CLIENT_PRESENCE_STATE_DESKTOP_IDLE, timeout_secs=int(10))
            )
        )
    except Exception as e:
        print(Fore.RED, "[ERROR] [module] [hangouts]", "Error in typing: ", e, Fore.RESET)


def set_presence(cid):
    global client
    request = hangups.hangouts_pb2.SetPresenceRequest(
        request_header=client.get_request_header(),
        presence_state_setting=hangouts_pb2.PresenceStateSetting(type=CLIENT_PRESENCE_STATE_DESKTOP_ACTIVE)
    )
    try:
        yield from client.set_presence(request)
    except Exception as e:
        print(Fore.RED, "[ERROR] [module] [hangouts]", "Error in presence: ", e, Fore.RESET)


def set_focus(cid):
    global client
    request = hangups.hangouts_pb2.SetFocusRequest(
        request_header=client.get_request_header(),
        conversation_id=hangups.hangouts_pb2.ConversationId(
            id=cid
        ),
        type=hangups.hangouts_pb2.FOCUS_TYPE_FOCUSED,
        timeout_secs=int(15),
    )
    try:
        yield from client.set_focus(request)
    except Exception as e:
        print(Fore.RED, "[ERROR] [module] [hangouts]", "Error in focussing: ", e, Fore.RESET)


def stayonline():
    while True:
        print("Setting focus")
        for cid in toseai.so_cid:
            asyncio.async(set_focus(cid))
        time.sleep(5)


def init():
    # Connect and authenticate
    global client
    cookies = hangups.auth.get_auth_stdin(REFRESH_TOKEN_PATH)
    client = hangups.Client(cookies)
    client.on_state_update.add_observer(on_state_update)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.connect())


if __name__ == '__main__':
    print(Fore.GREEN, "[hangouts]", "Running", Fore.RESET)
    init()
else:
    print(Fore.BLUE, "[module]",__name__, "module loaded", Fore.RESET)
